<?php

namespace AppBundle\Controller\Category;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Util\Codes;
use AppBundle\Rest\Representations\Category\GetCategoriesRepresentation;
use AppBundle\Rest\Representations\Category\GetCategoryRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Controller\RestController;

/**
 * Class CategoryController
 */
class CategoryController extends RestController
{
    /**
     * @Get("/categories")
     * @ParamConverter("categoryRepresentation", class="AppBundle\Rest\Representations\Category\GetCategoriesRepresentation")
     *
     * @param GetCategoriesRepresentation $categoriesRepresentation
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \AppBundle\Rest\RepresentationHandlers\Exceptions\SupportRepresentationException
     */
    public function getCategoriesAction(GetCategoriesRepresentation $categoriesRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $categories = $representationHandler->handle($categoriesRepresentation);

        return $this->handleView(
            $this->buildView(
                true,
                [
                    'result' => self::RESULT_SUCCESS,
                    'categories' => $categories,
                ],
                Codes::HTTP_OK
            )
        );
    }

    /**
     * @Get("/category/{categoryId}", requirements={"categoryId" = "\d+"})
     * @ParamConverter("categoryRepresentation", class="AppBundle\Rest\Representations\Category\GetCategoryRepresentation")
     *
     * @param GetCategoryRepresentation $categoryRepresentation
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \AppBundle\Rest\RepresentationHandlers\Exceptions\SupportRepresentationException
     */
    public function getCategoryAction(GetCategoryRepresentation $categoryRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $category = $representationHandler->handle($categoryRepresentation);

        return $this->handleView(
            $this->buildView(
                true,
                [
                    'result' => self::RESULT_SUCCESS,
                    'category' => $category,
                ],
                Codes::HTTP_OK
            )
        );
    }
}
