<?php
/**
 * Created by PhpStorm.
 * User: alexanderzheka
 * Date: 5/18/16
 * Time: 5:25 PM
 */

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Util\Codes;

class ClientController extends RestController
{

    /**
     * Authenticate user
     *
     * @Get("/me")
     *
     * @return View
     */
    public function meAction(){

        return $this->buildView(
            true,
            ['user'=>$this->getUser()],
            Codes::HTTP_OK
        );
    }

}