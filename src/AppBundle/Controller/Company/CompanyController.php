<?php

namespace AppBundle\Controller\Company;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Link;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Util\Codes;
use AppBundle\Rest\Representations\Company\PostCompanyRepresentation;
use AppBundle\Rest\Representations\Company\PatchCompanyRepresentation;
use AppBundle\Rest\Representations\Company\LinkCompanyRepresentation;
use AppBundle\Rest\Representations\Company\GetCompanyRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Controller\RestController;

/**
 * Class CompanyController
 */
class CompanyController extends RestController
{
    /**
     * @Post("/company")
     * @ParamConverter("companyRepresentation", class="AppBundle\Rest\Representations\Company\PostCompanyRepresentation")
     *
     * @param PostCompanyRepresentation $companyRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function createCompanyAction(PostCompanyRepresentation $companyRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $company = $representationHandler->handle($companyRepresentation);

        return $this->handleView(
            $this->buildView(
                true,
                [
                    'result' => self::RESULT_SUCCESS,
                    'companyId' => $company->getId(),
                ],
                Codes::HTTP_CREATED
            )
        );
    }

    /**
     * @Patch("/company/{companyId}", requirements={"companyId" = "\d+"})
     * @ParamConverter("patchCompanyRepresentation", class="AppBundle\Rest\Representations\Company\PatchCompanyRepresentation")
     *
     * @param PatchCompanyRepresentation $patchCompanyRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function patchCompanyAction(PatchCompanyRepresentation $patchCompanyRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $company = $representationHandler->handle($patchCompanyRepresentation);

        return $this->handleView(
            $this->buildView(true, ['result' => self::RESULT_SUCCESS], Codes::HTTP_OK)
        );
    }

    /**
     * @Link("/company/{companyId}", name="company", requirements={"companyId" = "\d+"})
     * @ParamConverter("linkCompanyRepresentation", class="AppBundle\Rest\Representations\Company\LinkCompanyRepresentation")
     *
     * @param LinkCompanyRepresentation $linkCompanyRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function linkAction(LinkCompanyRepresentation $linkCompanyRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');

        $representationHandler->handle($linkCompanyRepresentation);

        return $this->handleView(
            $this->buildView(
                true,
                [
                    'result' => self::RESULT_SUCCESS,
                ],
                Codes::HTTP_OK
            )
        );
    }

    /**
     * @Get("/company")
     * @ParamConverter("companyRepresentation", class="AppBundle\Rest\Representations\Company\GetCompanyRepresentation")
     *
     * @param GetCompanyRepresentation $companyRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function getCompanyAction(GetCompanyRepresentation $companyRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $company = $representationHandler->handle($companyRepresentation);

        return $this->handleView(
            $this->buildView(
                true,
                [
                    'result' => self::RESULT_SUCCESS,
                    'company' => $company,
                ],
                Codes::HTTP_OK
            )
        );
    }
}
