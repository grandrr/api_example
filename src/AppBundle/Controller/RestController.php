<?php

namespace AppBundle\Controller;

use Symfony\Component\EventDispatcher\Event;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\Form;


/**
 * Abstract controller
 *
 */
abstract class RestController extends FOSRestController
{

//    TODO! Change to i18n usage
    const MESS_ADD = 'Item has been added';
    const MESS_EDIT = 'Item has been changed';
    const MESS_DELETE = 'Item has been removed';
    const MESS_ERROR_ADD = 'Item addition error';
    const MESS_ERROR_EDIT = 'Item changing error';
    const MESS_ERROR_DELETE = 'Item removing error';

    const RESULT_SUCCESS = 'success';
    const RESULT_ERROR = 'error';

    protected function triggerEvent($type, Event $event)
    {
        $this->get('event_dispatcher')->dispatch($type, $event);
    }

    protected function buildView($result, $data = [], $status = 200, $header = [])
    {
        return parent::view([
            'result' => boolval($result),
            'status' => ($result == true ? 'ok' : 'error'),
            'details' => $data,
        ], $status, $header);
    }

    /**
     *
     * @param object $entity
     * @return \Symfony\Component\Validator\ConstraintViolationList
     */
    protected function assertValidateModel($entity)
    {
        return $this->get('validator')->validate($entity);
    }

    /**
     * @param Form $form
     * @return array
     */
    protected function getFormErrors(Form $form)
    {
        $errors = [];
        foreach ($form->all() as $fieldName => $formField) {
            foreach ($formField->getErrors(true) as $error) {
                $errors[$fieldName] = $error->getMessage();
            }
        }

        return $errors;
    }


    /**
     * @param $parameterName
     * @return array
     */
    protected function badValue($parameterName)
    {
//        TODO! should be hardcoded
        return [$parameterName => 'Bad value'];
    }
}
