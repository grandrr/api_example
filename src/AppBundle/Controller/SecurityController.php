<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Post;

class SecurityController extends RestController
{

    /**
     * Authenticate user
     *
     * @Post("/oauth/{tokenType}", requirements={"tokenType": "(token|fbtoken)"})
     *
     * @param Request $request
     * @return View
     */
    public function tokenAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $user->getUsername()]);

        return $this->buildView(
            true,
            ["token" => $token],
            Codes::HTTP_OK
        );
    }
}

