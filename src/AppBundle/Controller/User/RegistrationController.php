<?php

namespace AppBundle\Controller\User;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Util\Codes;
use AppBundle\Rest\Representations\User\PostUserRepresentation;
use AppBundle\Rest\Representations\User\PostSubUserRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Controller\RestController;

/**
 * Class RegistrationController
 */
class RegistrationController extends RestController
{
    /**
     * @Post("/registration")
     * @ParamConverter("postUserRepresentation", class="AppBundle\Rest\Representations\User\PostUserRepresentation")
     *
     * @param PostUserRepresentation $postUserRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function createUserAction(PostUserRepresentation $postUserRepresentation)
    {
        $postUserRepresentationHandler = $this->get('app.representation_handler');
        $user = $postUserRepresentationHandler->handle($postUserRepresentation);

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $user->getUsername()]);

        return $this->buildView(
            true,
            ['result' => self::RESULT_SUCCESS, 'token' => $token],
            Codes::HTTP_CREATED
        );
    }

    /**
     * @Post("/subuser")
     * @ParamConverter("postSubUserRepresentation", class="AppBundle\Rest\Representations\User\PostSubUserRepresentation")
     *
     * @param PostSubUserRepresentation $postSubUserRepresentation
     * @return \FOS\RestBundle\View\View
     */
    public function postSubUserAction(PostSubUserRepresentation $postSubUserRepresentation)
    {
        $representationHandler = $this->get('app.representation_handler');
        $user = $representationHandler->handle($postSubUserRepresentation);

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $user->getUsername()]);

        return $this->buildView(
            true,
            ['result' => self::RESULT_SUCCESS, 'token' => $token],
            Codes::HTTP_CREATED
        );
    }
}
