<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category\Category;

/**
 * Class CategoryFixtures
 */
class CategoryFixtures implements FixtureInterface
{

    const FAKE_CATEGORY_NAME1 = 'category 1';
    const FAKE_CATEGORY_NAME2 = 'category 2';
    const FAKE_CATEGORY_NAME3 = 'category 3';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('category 1');
        $category->setIsGoods(true);
        $manager->persist($category);

        $category = new Category();
        $category->setName('category 2');
        $category->setIsGoods(true);
        $manager->persist($category);

        $category = new Category();
        $category->setName('category 3');
        $category->setIsGoods(true);
        $manager->persist($category);

        $category = new Category();
        $category->setName('category 4');
        $category->setIsServices(true);
        $manager->persist($category);

        $category = new Category();
        $category->setName('category 5');
        $category->setIsServices(true);
        $manager->persist($category);

        $category = new Category();
        $category->setName('category 6');
        $category->setIsServices(true);
        $manager->persist($category);

        $manager->flush();
    }

}