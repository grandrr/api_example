<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Company\Company;
use AppBundle\Entity\User\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class CompanyFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class CompanyFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    const FAKE_COMPANY1 = 'company 1';
    const FAKE_COMPANY2 = 'company 2';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $fakeUser1 = $manager->getRepository('AppBundle:User\User')->findOneBy(array('username' => UserFixtures::FAKE_USERNAME1));

        $company = new Company();
        $company->setName(self::FAKE_COMPANY1);
        $company->setType(1);
        $company->setUser($fakeUser1);
        $manager->persist($company);

        $fakeUser2 = $manager->getRepository('AppBundle:User\User')->findOneBy(array('username' => UserFixtures::FAKE_USERNAME2));

        $company = new Company();
        $company->setName(self::FAKE_COMPANY2);
        $company->setType(1);
        $company->setUser($fakeUser2);
        $manager->persist($company);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

}