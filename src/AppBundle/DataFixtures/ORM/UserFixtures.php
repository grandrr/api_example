<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class UserFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     *
     */
    const FAKE_USERNAME1 = 'test@gmail.com';

    /**
     *
     */
    const FAKE_PASSWORD1 = 'testPass';

    /**
     *
     */
    const FAKE_USERNAME2 = 'fake2@gmail.com';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername(self::FAKE_USERNAME1);
        $user->setPassword(self::FAKE_PASSWORD1);
        $manager->persist($user);

        $user = new User();
        $user->setUsername(self::FAKE_USERNAME2);
        $user->setPassword("testPass");
        $manager->persist($user);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }

}