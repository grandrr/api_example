<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RepresentationHandleCompiler
 * @package AppBundle\DependencyInjection\Compiler
 */
class RepresentationHandleCompiler implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('app.representation_handler')) {
            return;
        }

        $definition = $container->findDefinition(
            'app.representation_handler'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'handler.representation'
        );
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addHandler',
                array(new Reference($id))
            );
        }
    }
}
