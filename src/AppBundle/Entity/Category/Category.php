<?php

namespace AppBundle\Entity\Category;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Table(name="Category")
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentId", type="smallint", nullable=true)
     */
    protected $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_services", type="boolean", nullable=false, options={"default"=0})
     */
    protected $isServices = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_goods", type="boolean", nullable=false, options={"default"=0})
     */
    protected $isGoods = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param $parentId
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsServices()
    {
        return $this->isServices;
    }

    /**
     * @param $isServices
     * @return $this
     */
    public function setIsServices($isServices)
    {
        $this->isServices = $isServices;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsGoods()
    {
        return $this->isGoods;
    }

    /**
     * @param $isGoods
     * @return $this
     */
    public function setIsGoods($isGoods)
    {
        $this->isGoods = $isGoods;

        return $this;
    }
}
