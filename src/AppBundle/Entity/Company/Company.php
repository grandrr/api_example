<?php

namespace AppBundle\Entity\Company;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Category\Category;
use AppBundle\Entity\Resource\Resource;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\Offer\Offer;

/**
 *
 * @ORM\Table(name="Company")
 * @ORM\Entity
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category\Category")
     * @ORM\JoinTable(name="Company_categories",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    protected $categories;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyPhone", type="string", length=25, nullable=true)
     */
    protected $verifyPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyName", type="string", length=255, nullable=true)
     */
    protected $verifyName;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyExtra", type="string", length=255, nullable=true)
     */
    protected $verifyExtra;

    /**
     * @var string
     *
     * @ORM\Column(name="verifyType", type="string", length=255, nullable=true)
     */
    protected $verifyType;

    /**
     * @var string
     *
     * @ORM\Column(name="isVerified", type="boolean")
     */
    protected $isVerified = 0;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getVerifyPhone()
    {
        return $this->verifyPhone;
    }

    /**
     * @param $verifyPhone
     * @return $this
     */
    public function setVerifyPhone($verifyPhone)
    {
        $this->verifyPhone = $verifyPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getVerifyName()
    {
        return $this->verifyName;
    }

    /**
     * @param $verifyName
     * @return $this
     */
    public function setVerifyName($verifyName)
    {
        $this->verifyName = $verifyName;

        return $this;
    }

    /**
     * @return string
     */
    public function getVerifyExtra()
    {
        return $this->verifyExtra;
    }

    /**
     * @param $verifyExtra
     * @return $this
     */
    public function setVerifyExtra($verifyExtra)
    {
        $this->verifyExtra = $verifyExtra;

        return $this;
    }

    /**
     * @return string
     */
    public function getVerifyType()
    {
        return $this->verifyType;
    }

    /**
     * @param $verifyType
     * @return $this
     */
    public function setVerifyType($verifyType)
    {
        $this->verifyType = $verifyType;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param $isVerified
     * @return $this
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }
}

