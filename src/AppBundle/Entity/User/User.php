<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Expose;


/**
 *
 * @ORM\Table(name="User")
 * @ORM\Entity
 */
class User implements UserInterface
{

    /**
     * @var integer
     *
     * @Expose
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @Expose
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Expose
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * @var integer
     *
     * @Expose
     *
     * @ORM\Column(name="avatar", type="smallint", nullable=true)
     */
    protected $avatar;

    /**
     * @var string
     *
     * @Expose
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    protected $position;

    /**
     * @var string

     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id", nullable=true)
     */
    protected $parentUser;

    /**
     * @var string
     *
     * @ORM\Column(name="is_seller", type="boolean", nullable=true)
     * @Assert\NotNull(message = "isSeller can not be null")
     */
    protected $isSeller;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", nullable=false)
     * @Assert\NotNull(message = "username can not be null")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     * @Assert\NotNull(message = "password can not be null")
     */
    protected $password;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Company\Company", mappedBy="user")
     */
    protected $company;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parentId = 0;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
       return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set avatar
     *
     * @param integer $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return integer
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param User $parentUser
     * @return $this
     */
    public function setParentUser(User $parentUser)
    {
        $this->parentUser = $parentUser;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentUser()
    {
        return $this->parentUser;
    }

    /**
     * Set isSeller
     *
     * @param boolean $isSeller
     *
     * @return User
     */
    public function setIsSeller($isSeller)
    {
        $this->isSeller = $isSeller;

        return $this;
    }

    /**
     * Get isSeller
     *
     * @return boolean
     */
    public function getIsSeller()
    {
        return $this->isSeller;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }
}
