<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use FOS\RestBundle\View\View;
use AppBundle\Exception;

class ExceptionListener
{

    use ContainerAwareTrait;

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        $logger = $this->container->get('logger');

        $message = sprintf(
            '%s: %s (uncaught exception) at %s line %s',
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        $logger->debug($message, ['exception' => $exception]);

        $logger->error($exception->getMessage());
        $logger->error($exception->getTraceAsString());

        if ($exception instanceof \Exception) {
            $data = [
                'result'  => false,
                'status'  => 'error',
                'details' => [
                    'message' => $exception->getMessage()
                ],
            ];

            // Customize your response object to display the exception details
            $view = View::create($data, $exception->getCode(), []);
            $response = $this->container->get('fos_rest.view_handler')->handle($view);
            $event->setResponse($response);

            $event->stopPropagation();
        }
    }

}
