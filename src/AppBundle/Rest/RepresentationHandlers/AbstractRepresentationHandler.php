<?php

namespace AppBundle\Rest\RepresentationHandlers;

use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class AbstractRepresentationHandler
 */
abstract class AbstractRepresentationHandler implements RepresentationHandlerInterface
{
    /**
     * @var string
     */
    protected $className;

    /**
     * AbstractRepresentationHandler constructor.
     * @param string $className
     */
    public function __construct($className)
    {
        $this->className = $className;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return bool
     */
    public function supports(AbstractRepresentation $representation)
    {
        return $this->className === get_class($representation);
    }
}