<?php

namespace AppBundle\Rest\RepresentationHandlers\Category;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Category\Category;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class GetCategoriesRepresentationHandler
 */
class GetCategoriesRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Router
     */
    protected $router;

    /**
     * GetCategoriesRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     * @param Router        $router
     */
    public function __construct($className, EntityManager $entityManager, Router $router)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return array
     */
    public function handle(AbstractRepresentation $representation)
    {
        $categoriesRepository = $this->entityManager->getRepository('AppBundle:Category\Category');

        if (!empty($representation->getAllParams())) {
            $categories = $categoriesRepository
                ->findBy(
                    array(
                        'isGoods' => $representation->getIsGoods(),
                        'isServices' => $representation->getIsServices(),
                    )
                );
        } else {
            $categories = $categoriesRepository->findAll();
        }

        return $this->generateRepresentationData($categories);
    }

    /**
     * @param $categories
     * @return array
     */
    protected function generateRepresentationData($categories)
    {
        $result = array();

        foreach ($categories as $category) {

            /**
             * @var $category Category
             */

            $uri = $this->getUri($category);

            $result[] = array(
                'id' => $category->getId(),
                'name' => $category->getName(),
                'isServices' => $category->getIsServices(),
                'isGoods' => $category->getIsGoods(),
                'uri' => $uri,
            );
        }

        return $result;
    }

    /**
     * @param Category $category
     * @return string
     */
    protected function getUri(Category $category)
    {
        return $this->router->generate('get_category', array('categoryId' => $category->getId()));
    }

}