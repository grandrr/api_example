<?php

namespace AppBundle\Rest\RepresentationHandlers\Category;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Category\Category;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;

/**
 * Class GetCategoryRepresentationHandler
 */
class GetCategoryRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * GetCategoriesRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     */
    public function __construct($className, EntityManager $entityManager)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return Category[]|array
     */
    public function handle(AbstractRepresentation $representation)
    {
        $category = $this->entityManager->getRepository('AppBundle:Category\Category')
            ->findBy($representation->getCategoryId());

        return $category;
    }

}