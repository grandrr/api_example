<?php

namespace AppBundle\Rest\RepresentationHandlers\Company;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Company\Company;
use AppBundle\Entity\Category\Category;
use AppBundle\Entity\Resource\Resource;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class GetCompanyRepresentationHandler
 */
class GetCompanyRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var Router
     */
    protected $router;

    /**
     * GetCompanyRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     * @param TokenStorage  $tokenStorage
     * @param Router        $router
     */
    public function __construct($className, EntityManager $entityManager, TokenStorage $tokenStorage, Router $router)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return Company
     */
    public function handle(AbstractRepresentation $representation)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $entity = $this->entityManager->getRepository('AppBundle:Company\Company')->findOneBy(
            array('user' => $user)
        );

        return $this->generateRepresentationData($entity);
    }

    /**
     * @param Company $company
     * @return Company|array
     */
    protected function generateRepresentationData(Company $company)
    {
        $categories = array();

        if ($categoriesEntity = $company->getCategories()) {

            /**
             * @var $category Category
             */
            foreach ($categoriesEntity as $category) {
                $uri = $this->getCategoryUri($category);

                $categories[] = array(
                    'id' => $category->getId(),
                    'name' => $category->getName(),
                    'isServices' => $category->getIsServices(),
                    'isGoods' => $category->getIsGoods(),
                    'uri' => $uri,
                );
            }
        }


        $company = array(
            'id' => $company->getId(),
            'name' => $company->getName(),
            'type' => $company->getType(),
            'description' => $company->getDescription(),
            'url' => $company->getUrl(),
            'categories' => $categories,
        );

        return $company;
    }

    /**
     * !todo move to better place
     * @param Category $category
     * @return string
     */
    protected function getCategoryUri(Category $category)
    {
        return $this->router->generate('get_category', array('categoryId' => $category->getId()));
    }

}