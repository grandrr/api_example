<?php

namespace AppBundle\Rest\RepresentationHandlers\Company;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Company\Company;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;

/**
 * Class LinkCompanyRepresentationHandler
 */
class LinkCompanyRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * PostCompanyRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     */
    public function __construct($className, EntityManager $entityManager)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return Company
     */
    public function handle(AbstractRepresentation $representation)
    {
        $company = $this->entityManager->getRepository('AppBundle:Company\Company')->find(
            $representation->getCompanyId()
        );

        $this->handleLogo($company, $representation->getLogos());
        $this->handleCategory($company, $representation->getCategories());

        $this->entityManager->persist($company);
        $this->entityManager->flush($company);

        return $company;
    }

    /**
     * @param Company $company
     * @param $resourcesId
     * @return Company
     */
    protected function handleLogo(Company $company, $resourcesId)
    {
        if (count($resourcesId) > 0) {
            $resource = $this->entityManager->getRepository('AppBundle:Resource\Resource')->find($resourcesId[0]);
            $company->setResourceLogo($resource);
        }

        return $company;
    }

    /**
     * @param Company $company
     * @param $categoriesId
     * @return Company
     */
    protected function handleCategory(Company $company, $categoriesId)
    {
        if (count($categoriesId) > 0) {
            $categories = $this->entityManager->getRepository('AppBundle:Category\Category')->findById(
                $categoriesId
            );

            foreach ($categories as $category) {
                $company->addCategory($category);
            }

        }

        return $company;
    }

}