<?php

namespace AppBundle\Rest\RepresentationHandlers\Company;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Company\Company;
use AppBundle\Rest\Representations\Company\PatchCompanyRepresentation;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;

/**
 * Class PatchCompanyRepresentationHandler
 */
class PatchCompanyRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * PostCompanyRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     */
    public function __construct($className, EntityManager $entityManager)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return Company
     */
    public function handle(AbstractRepresentation $representation)
    {
        /**
         * @var $representation PatchCompanyRepresentation
         */
        $companyId = $representation->getCompanyId();
        $verifyPhone = $representation->getVerifyPhone();
        $verifyName = $representation->getVerifyName();
        $verifyExtra = $representation->getVerifyExtra();
        $verifyType = $representation->getVerifyType();

        $entity = $this->entityManager->getRepository('AppBundle:Company\Company')->find($companyId);

        $entity->setVerifyPhone($verifyPhone);
        $entity->setVerifyName($verifyName);
        $entity->setVerifyExtra($verifyExtra);
        $entity->setVerifyType($verifyType);

        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);

        return $entity;
    }

}