<?php

namespace AppBundle\Rest\RepresentationHandlers\Company;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\Company\Company;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PostCompanyRepresentationHandler
 */
class PostCompanyRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * PostCompanyRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     * @param TokenStorage  $tokenStorage
     */
    public function __construct($className, EntityManager $entityManager, TokenStorage $tokenStorage)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return Company
     */
    public function handle(AbstractRepresentation $representation)
    {
        $name = $representation->getName();
        $type = $representation->getType();
        $description = $representation->getDescription();
        $url = $representation->getUrl();
        $user = $this->tokenStorage->getToken()->getUser();

        $entity = new Company();

        $entity->setName($name);
        $entity->setType($type);
        $entity->setDescription($description);
        $entity->setUrl($url);
        $entity->setUser($user);

        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);

        return $entity;
    }

}