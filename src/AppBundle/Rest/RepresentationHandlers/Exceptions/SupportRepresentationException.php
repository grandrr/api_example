<?php

namespace AppBundle\Rest\RepresentationHandlers\Exceptions;

/**
 * Class SupportRepresentationException
 */
class SupportRepresentationException extends \Exception
{
    /**
     * ExceptionSupportRepresentation constructor.
     * @param string          $representationClass
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct($representationClass, $code = 0, \Exception $previous = null)
    {
        $message = sprintf('Not found representation handler for representation %s', $representationClass);

        parent::__construct($message, $code, $previous);
    }

}