<?php

namespace AppBundle\Rest\RepresentationHandlers;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Rest\RepresentationHandlers\Exceptions\SupportRepresentationException;

/**
 * Class RepresentationHandler
 */
class RepresentationHandlerChain
{

    /**
     * @var array
     */
    protected $handlers = array();

    /**
     * @param RepresentationHandlerInterface $handler
     */
    public function addHandler(RepresentationHandlerInterface $handler)
    {
        $this->handlers[] = $handler;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return mixed
     * @throws SupportRepresentationException
     */
    public function handle(AbstractRepresentation $representation)
    {
        /**
         * @var $handler RepresentationHandlerInterface
         */

        foreach ($this->handlers as $handler) {
            if ($handler->supports($representation)) {
                return $handler->handle($representation);

            }

        }

        throw new SupportRepresentationException(get_class($representation));

    }

}