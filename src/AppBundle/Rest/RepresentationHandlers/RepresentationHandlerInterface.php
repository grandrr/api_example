<?php

namespace AppBundle\Rest\RepresentationHandlers;

use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Interface RepresentationHandlerInterface
 */
interface RepresentationHandlerInterface
{

    /**
     * @param AbstractRepresentation $representation
     * @return mixed
     */
    public function handle(AbstractRepresentation $representation);

    /**
     * @param AbstractRepresentation $representation
     * @return mixed
     */
    public function supports(AbstractRepresentation $representation);

}