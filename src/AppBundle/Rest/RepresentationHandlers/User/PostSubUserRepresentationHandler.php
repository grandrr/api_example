<?php

namespace AppBundle\Rest\RepresentationHandlers\User;

use AppBundle\Rest\Representations\AbstractRepresentation;
use AppBundle\Entity\User\User;
use Doctrine\ORM\EntityManager;
use AppBundle\Rest\RepresentationHandlers\AbstractRepresentationHandler;

/**
 * Class PostSubUserRepresentationHandler
 */
class PostSubUserRepresentationHandler extends AbstractRepresentationHandler
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * PostCompanyRepresentationHandler constructor.
     * @param string        $className
     * @param EntityManager $entityManager
     */
    public function __construct($className, EntityManager $entityManager)
    {
        parent::__construct($className);
        $this->entityManager = $entityManager;
    }

    /**
     * @param AbstractRepresentation $representation
     * @return User
     */
    public function handle(AbstractRepresentation $representation)
    {
        $username = $representation->getUsername();
        $password = $representation->getPassword();
        $parentUser = $this->entityManager->getRepository('AppBundle:User\User')->find(
            $representation->getParentUser()
        );

        $user = new User();

        $user->setUsername($username);
        $user->setPassword($password);
        $user->setParentUser($parentUser);
        $user->setIsSeller(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;

    }

}