<?php

namespace AppBundle\Rest\RepresentationParamConverters;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractRepresentationParamConverter
 */
abstract class AbstractRepresentationParamConverter implements ParamConverterInterface
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var
     */
    protected $targetRepresentation;

    /**
     * AbstractRepresentationParamConverter constructor.
     * @param $targetRepresentation
     * @param ValidatorInterface $validator
     */
    public function __construct($targetRepresentation, ValidatorInterface $validator)
    {
        $this->targetRepresentation = $targetRepresentation;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $representationClass = $configuration->getClass();

        $postUserRepresentation = new $representationClass();

        $requestParams = $this->fetchParams($request);

        foreach ($requestParams as $paramName => $paramValue) {
            $postUserRepresentation->$paramName = $paramValue;
        }

        $errors = $this->validator->validate($postUserRepresentation);

        if ($errors->count() > 0) {
            throw new NotAcceptableHttpException($errors);
        }

        $param = $configuration->getName();

        $request->attributes->set($param, $postUserRepresentation);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return $this->checkSupports($configuration);
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    protected function checkSupports(ParamConverter $configuration)
    {
        return $this->targetRepresentation === $configuration->getClass();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    abstract protected function fetchParams(Request $request);

}