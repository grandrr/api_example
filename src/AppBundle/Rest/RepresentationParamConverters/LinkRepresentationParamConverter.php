<?php

namespace AppBundle\Rest\RepresentationParamConverters;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class LinkRepresentationParamConverter
 */
class LinkRepresentationParamConverter extends AbstractRepresentationParamConverter
{

    /**
     * @param Request $request
     * @return array
     */
    protected function fetchParams(Request $request)
    {
        $requestData = $request->attributes->all();

        $links = $this->fetchLinks($request->headers->get('link'));

        return array_merge($requestData, $links);
    }

    /**
     * @param $linksHeader
     * @return array
     */
    protected function fetchLinks($linksHeader)
    {
        $result = array();

        $links = explode(',', $linksHeader);

        foreach ($links as $link) {
            $resourceId = $this->fetchResourceId($link);
            $relation = $this->fetchRelation($link);

            if ($resourceId && $relation) {
                $result[$relation][] = $resourceId;

            }

        }

        return $result;
    }

    /**
     * @param $link
     * @return mixed
     */
    protected function fetchResourceId($link)
    {
        preg_match('/\/([a-zA-Z0-9]+)>;/', $link, $resourceId);

        if (isset($resourceId[1])) {
            return $resourceId[1];
        }

        return false;
    }

    /**
     * @param $link
     * @return bool
     */
    protected function fetchRelation($link)
    {
        preg_match('/rel="(.+)"/', $link, $relation);

        if (isset($relation[1])) {
            return $relation[1];
        }

        return false;
    }

}