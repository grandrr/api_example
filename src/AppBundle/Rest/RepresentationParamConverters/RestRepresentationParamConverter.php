<?php

namespace AppBundle\Rest\RepresentationParamConverters;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class RestRepresentationParamConverter
 */
class RestRepresentationParamConverter extends AbstractRepresentationParamConverter
{

    /**
     * @param Request $request
     * @return array
     */
    protected function fetchParams(Request $request)
    {
        $requestData = array_merge($request->request->all(), $request->query->all(), $request->attributes->all());

        return $requestData;
    }
}
