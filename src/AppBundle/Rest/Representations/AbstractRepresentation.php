<?php

namespace AppBundle\Rest\Representations;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractRepresentation
 */
abstract class AbstractRepresentation
{
    /**
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }

    /**
     * @return array
     */
    public function getAllParams()
    {
        $vars = get_object_vars($this);

        $result = array_filter($vars, 'self::filterEmptyResults');

        return $result;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function filterEmptyResults($value)
    {
        return $value !== null;
    }
}