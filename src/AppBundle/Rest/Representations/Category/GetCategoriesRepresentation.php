<?php

namespace AppBundle\Rest\Representations\Category;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetCategoriesRepresentation
 */
class GetCategoriesRepresentation extends AbstractRepresentation
{
    /**
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isGoods;

    /**
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $isServices;

    /**
     * @return mixed
     */
    public function getIsGoods()
    {
        return $this->isGoods;
    }

    /**
     * @return mixed
     */
    public function getIsServices()
    {
        return $this->isServices;
    }
}