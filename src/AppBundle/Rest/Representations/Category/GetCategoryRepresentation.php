<?php

namespace AppBundle\Rest\Representations\Category;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetCategoryRepresentation
 */
class GetCategoryRepresentation extends AbstractRepresentation
{
    /**
     * @Type("integer")
     * @Assert\NotBlank(message="Please enter categoryId value")
     */
    protected $categoryId;

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

}