<?php

namespace AppBundle\Rest\Representations\City;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetCitiesRepresentation
 */
class GetCitiesRepresentation extends AbstractRepresentation
{
    /**
     * @var
     */
    protected $id;

    /**
     * @var
     */
    protected $title;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

}