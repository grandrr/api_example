<?php

namespace AppBundle\Rest\Representations\Company;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetCompanyRepresentation
 */
class GetCompanyRepresentation extends AbstractRepresentation
{
}