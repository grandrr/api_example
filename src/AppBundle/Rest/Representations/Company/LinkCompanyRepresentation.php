<?php

namespace AppBundle\Rest\Representations\Company;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class LinkCompanyRepresentation
 */
class LinkCompanyRepresentation extends AbstractRepresentation
{
    /**
     * !todo check company exist
     */

    /**
     * @Assert\NotBlank(message="Please enter company id")
     */
    protected $companyId;

    /**
     * !todo check resource exist
     */

    /**
     * @var array
     */
    protected $logos = array();

    /**
     * @var array
     */
    protected $categories = array();

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return array
     */
    public function getLogos()
    {
        return $this->logos;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

}