<?php

namespace AppBundle\Rest\Representations\Company;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PatchCompanyRepresentation
 */
class PatchCompanyRepresentation extends AbstractRepresentation
{

    /**
     * !todo add check is user owner of company
     */
    /**
     * @Type("integer")
     * @Assert\NotBlank(message="Please enter company id")
     */
    protected $companyId;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter phone")
     */
    protected $verifyPhone;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter name")
     */
    protected $verifyName;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter extra")
     */
    protected $verifyExtra;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter type")
     */
    protected $verifyType;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return mixed
     */
    public function getVerifyPhone()
    {
        return $this->verifyPhone;
    }

    /**
     * @return mixed
     */
    public function getVerifyName()
    {
        return $this->verifyName;
    }

    /**
     * @return mixed
     */
    public function getVerifyExtra()
    {
        return $this->verifyExtra;
    }

    /**
     * @return mixed
     */
    public function getVerifyType()
    {
        return $this->verifyType;
    }
}