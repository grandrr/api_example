<?php

namespace AppBundle\Rest\Representations\Company;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostUserRepresentation
 */
class PostCompanyRepresentation extends AbstractRepresentation
{
    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter name")
     */
    protected $name;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter type")
     */
    protected $type;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter description")
     */
    protected $description;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter url")
     */
    protected $url;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }
}