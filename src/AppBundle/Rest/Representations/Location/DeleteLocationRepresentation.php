<?php

namespace AppBundle\Rest\Representations\Location;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class DeleteLocationRepresentation
 */
class DeleteLocationRepresentation extends AbstractRepresentation
{
    /**
     * @Assert\NotBlank(message="Please enter location id")
     */
    protected $locationId;

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

}