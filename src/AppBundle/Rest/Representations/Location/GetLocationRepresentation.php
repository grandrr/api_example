<?php

namespace AppBundle\Rest\Representations\Location;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetLocationRepresentation
 */
class GetLocationRepresentation extends AbstractRepresentation
{
    /**
     * @var
     */
    protected $locationId;

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

}