<?php

namespace AppBundle\Rest\Representations\Location;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetLocationsRepresentation
 */
class GetLocationsRepresentation extends AbstractRepresentation
{
    /**
     * @var
     */
    protected $id;

    /**
     * @var
     */
    protected $title;

    /**
     * @var
     */
    protected $city;

    /**
     * @var
     */
    protected $address;

    /**
     * @var
     */
    protected $description;

    /**
     * @var
     */
    protected $schedule;

    /**
     * @var
     */
    protected $workingDays;

    /**
     * @var
     */
    protected $isDeleted;

    /**
     * @var
     */
    protected $isPublished;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @return mixed
     */
    public function getWorkingDays()
    {
        return $this->workingDays;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @return mixed
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

}