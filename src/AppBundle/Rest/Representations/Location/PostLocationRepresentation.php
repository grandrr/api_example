<?php

namespace AppBundle\Rest\Representations\Location;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostLocationRepresentation
 */
class PostLocationRepresentation extends AbstractRepresentation
{
    /**
     * @Assert\NotBlank(message="Please enter title")
     */
    protected $title;

    /**
     * @Assert\NotBlank(message="Please enter city id")
     */
    protected $cityId;

    /**
     * @Assert\NotBlank(message="Please enter address")
     */
    protected $address;

    /**
     * @Assert\NotBlank(message="Please enter description")
     */
    protected $description;

    /**
     * @Assert\NotBlank(message="Please enter schedule")
     */
    protected $schedule;

    /**
     * @Assert\NotBlank(message="Please enter working days")
     */
    protected $workingDays;

    /**
     * @Assert\NotBlank(message="Please enter is deleted")
     */
    protected $isDeleted;

    /**
     * @Assert\NotBlank(message="Please enter is published")
     */
    protected $isPublished;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @return mixed
     */
    public function getWorkingDays()
    {
        return $this->workingDays;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @return mixed
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
}