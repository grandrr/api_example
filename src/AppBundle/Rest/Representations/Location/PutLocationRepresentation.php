<?php

namespace AppBundle\Rest\Representations\Location;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PutLocationRepresentation
 */
class PutLocationRepresentation extends PostLocationRepresentation
{
    /**
     * @Assert\NotBlank(message="Please enter location id")
     */
    protected $locationId;

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }
}