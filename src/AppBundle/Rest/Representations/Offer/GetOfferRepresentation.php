<?php

namespace AppBundle\Rest\Representations\Offer;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetOfferRepresentation
 */
class GetOfferRepresentation extends AbstractRepresentation
{
    /**
     * @Assert\NotBlank(message="Please enter offerId value")
     */
    protected $offerId;

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offerId;
    }
}