<?php

namespace AppBundle\Rest\Representations\Offer;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetOffersRepresentation
 */
class GetOffersRepresentation extends AbstractRepresentation
{
    /**
     * !todo check exist company
     * @var
     */
    protected $companies;

    /**
     * !todo check exist category
     * @var
     */
    protected $categories;

    /**
     * !todo check exist city
     * @var
     */
    protected $citiesId;

    /**
     * !todo check exist city
     * @var
     */
    protected $citiesName;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $offset;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $limit;

    /**
     * @return mixed
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return mixed
     */
    public function getCitiesId()
    {
        return $this->citiesId;
    }

    /**
     * @return mixed
     */
    public function getCitiesName()
    {
        return $this->citiesName;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }
}