<?php

namespace AppBundle\Rest\Representations\Offer;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class LinkOfferRepresentation
 */
class LinkOfferRepresentation extends AbstractRepresentation
{
    /**
     * !todo check offer exist
     */

    /**
     * @Assert\NotBlank(message="Please enter offer id")
     */
    protected $offerId;

    /**
     * !todo check resource logo exist
     */
    protected $logos = array();

    /**
     * !todo check resource image exist
     * @var array
     */
    protected $images = array();

    /**
     * !todo check category exist
     * @var array
     */
    protected $categories = array();

    /**
     * !todo check location exist
     * @var array
     */
    protected $locations = array();

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @return array
     */
    public function getLogos()
    {
        return $this->logos;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function getLocations()
    {
        return $this->locations;
    }

}