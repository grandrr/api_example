<?php

namespace AppBundle\Rest\Representations\Offer;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PatchOfferRepresentation
 */
class PatchOfferRepresentation extends AbstractRepresentation
{

    /**
     * !todo add check is user owner of offer / offer exist
     */
    /**
     * @Assert\NotBlank(message="Please enter company id")
     */
    protected $offerId;

    /**
     * @Assert\NotBlank(message="Please enter phone")
     */
    protected $published;

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

}