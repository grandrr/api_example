<?php

namespace AppBundle\Rest\Representations\Offer;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostOfferRepresentation
 */
class PostOfferRepresentation extends AbstractRepresentation
{
    /**
     * !todo check if user has company
     */

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter name")
     */
    protected $title;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter type")
     */
    protected $type;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter description")
     */
    protected $description;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter url")
     */
    protected $url;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter date from")
     */
    protected $dateFrom;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter date to")
     */
    protected $dateTo;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter kind of")
     */
    protected $kindOf;

    /**
     * @Type("boolean")
     * @Assert\NotBlank(message="Please enter is published")
     */
    protected $isPublished;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @return mixed
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @return mixed
     */
    public function getKindOf()
    {
        return $this->kindOf;
    }

    /**
     * @return mixed
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
}