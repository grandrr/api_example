<?php

namespace AppBundle\Rest\Representations\Resource;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class GetResourceRepresentation
 */
class GetResourceRepresentation extends AbstractRepresentation
{
    /**
     * @Type("integer")
     * @Assert\NotBlank(message="Please enter resource id")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}