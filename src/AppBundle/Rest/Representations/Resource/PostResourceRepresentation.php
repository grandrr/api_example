<?php

namespace AppBundle\Rest\Representations\Resource;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostUserRepresentation
 */
class PostResourceRepresentation extends AbstractRepresentation
{
    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter resource content")
     */
    protected $content;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }
}