<?php

namespace AppBundle\Rest\Representations\User;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostSubUserRepresentation
 */
class PostSubUserRepresentation extends AbstractRepresentation
{
    /**
     * @Assert\NotBlank(message="Please enter password")
     */
    protected $password;

    /**
     * @Assert\NotBlank(message="Please enter username")
     */
    protected $username;


    /**
     *!todo check user exist
     */
    /**
     * @Assert\NotBlank(message="Please enter parent user")
     */
    protected $parentUser;

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getParentUser()
    {
        return $this->parentUser;
    }
}