<?php

namespace AppBundle\Rest\Representations\User;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Rest\Representations\AbstractRepresentation;

/**
 * Class PostUserRepresentation
 */
class PostUserRepresentation extends AbstractRepresentation
{
    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter password")
     */
    protected $password;

    /**
     * @Type("string")
     * @Assert\NotBlank(message="Please enter username")
     */
    protected $username;

    /**
     * @Type("boolean")
     */
    protected $isSeller;

    /**
     * @return mixed
     */
    public function getIsSeller()
    {
        return $this->isSeller;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
}