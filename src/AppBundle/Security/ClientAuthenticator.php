<?php

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class ClientAuthenticator
 * @package AppBundle\Security
 */
class ClientAuthenticator extends AbstractGuardAuthenticator implements NagodaClientAuthenticator
{
    private $em;

    private $clientKey;

    private $clientSecret;

    /**
     * ClientAuthenticator constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     * @param Request $request
     * @return array|null
     */
    public function getCredentials(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return null;
        }

        if (!$this->isRequestAuth($request)) {
            return null;
        }

        $credentials = [
            'username'    => $request->request->get('username'),
            'password'    => $request->request->get('password'),
        ];

        return $credentials;
    }

    /**
     * @param mixed                 $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByUsername($credentials['username']);
    }

    /**
     * @param mixed         $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        //todo: check password via encoder_factory
        //todo: if it's social auth - don't check password out
        return true;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     * @return mixed
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, 403);
    }


    /**
     * Called when authentication is needed, but it's not sent
     * @param Request                      $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = ['message' => 'Authentication Required'];

        return new JsonResponse($data, 401);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * Check whether client credentials are present
     * @param Request $request
     * @return bool
     */
    public function isRequestAuth(Request $request)
    {
        $clientKey = $request->request->get(static::API_KEY);
        $clientSecret = $request->request->get(static::API_SECRET);

        if (!$clientKey || $clientKey != $this->clientKey) {
            throw new AccessDeniedHttpException('This action needs a client key');
        }

        if (!$clientSecret || $clientSecret != $this->clientSecret) {
            throw new AccessDeniedHttpException('This action needs a client secret');
        }

        return true;
    }

    /**
     * @param string $apiKey
     */
    public function setClientKey($apiKey)
    {
        $this->clientKey = $apiKey;
    }

    /**
     * @param string $apiSecret
     */
    public function setClientSecret($apiSecret)
    {
        $this->clientSecret = $apiSecret;
    }
}
