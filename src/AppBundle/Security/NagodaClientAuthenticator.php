<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 5/9/16
 * Time: 3:59 PM
 */

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;

interface NagodaClientAuthenticator
{
    const API_KEY = "app_id";

    const API_SECRET = "app_secret";

    public function isRequestAuth(Request $request);

    public function setClientKey($apiKey);

    public function setClientSecret($apiSecret);
}