<?php

namespace UserBundle\Controller\Category;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class CategoryControllerTest
 * @package UserBundle\Controller\Category
 */
class CategoryControllerTest extends WebTestCase
{

    /**
     * @var Client
     */
    protected $client;

    /**
     *
     */
    public function setUp()
    {
        static::runCommand('doctrine:schema:drop', array('--force' => true));
        static::runCommand('doctrine:schema:create', array());
        static::runCommand('doctrine:fixtures:load', array('--env' => 'test'));

        $this->client = static::createClient();
    }

    /**
     *
     */
    public function testGetCategoriesAction()
    {
        $data = array(
            'isGoods' => 0,
            'isServices' => 1,
        );

        $this->client->request('GET', '/categories', $data);

        $this->assertEquals(Codes::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $content = json_decode($this->client->getResponse()->getContent());

        $this->assertObjectHasAttribute('categories', $content->details);

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testGetCategoriesWithErrorAction()
    {
        $data = array(
            'isGoods' => '',
            'isServices' => '',
        );

        $this->client->request('GET', '/categories', $data);

        $this->assertEquals(Codes::HTTP_NOT_ACCEPTABLE, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }
}