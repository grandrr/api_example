<?php

namespace UserBundle\Controller\Company;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use AppBundle\DataFixtures\ORM\UserFixtures;
use AppBundle\Security\NagodaClientAuthenticator;

/**
 * Class CompanyControllerTest
 */
class CompanyControllerTest extends WebTestCase
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var
     */
    protected $token;

    /**
     *
     */
    public function setUp()
    {
        static::runCommand('doctrine:schema:drop', array('--force' => true, '--env' => 'test'));
        static::runCommand('doctrine:schema:create', array('--env' => 'test'));
        static::runCommand('doctrine:fixtures:load', array('--env' => 'test'));

        $this->client = static::createClient();

        $data = array(
            'username' => UserFixtures::FAKE_USERNAME1,
            'password' => UserFixtures::FAKE_PASSWORD1,
            NagodaClientAuthenticator::API_KEY => 'test_api_key',
            NagodaClientAuthenticator::API_SECRET => 'test_api_secret',
        );

        $this->client->request('POST', '/oauth/token', $data);

        $content = json_decode($this->client->getResponse()->getContent());

        $this->token = $content->details->token;
    }

    /**
     *
     */
    public function testPostCreateCompanyAction()
    {
        $data = array(
            'name' => 'test name',
            'type' => 1,
            'description' => 'test description',
            'url' => 'http://test_url',
        );

        $this->client->request(
            'POST',
            '/company',
            $data,
            array(),
            array(
                'HTTP_Authorization' => array(
                    'token' => 'Bearer '.$this->token,
                ),
            )
        );

        $this->assertEquals(Codes::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        $result = json_decode($this->client->getResponse()->getContent());

        $this->assertObjectHasAttribute('companyId', $result->details);
    }

    /**
     *
     */
    public function testPostCreateCompanyWithErrorAction()
    {
        $data = array(
            'name' => '',
            'type' => 1,
            'description' => 'test description',
            'url' => 'http://test_url',
        );

        $this->client->request(
            'POST',
            '/company',
            $data,
            array(),
            array(
                'HTTP_Authorization' => array(
                    'token' => 'Bearer '.$this->token,
                ),
            )
        );

        $this->assertEquals(Codes::HTTP_NOT_ACCEPTABLE, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testPatchCompanyAction()
    {
        $data = array(
            'verifyPhone' => '0000000000',
            'verifyName' => 'test verify name',
            'verifyExtra' => 'test verify name',
            'verifyType' => 'test verify name',
        );

        $this->client->request(
            'PATCH',
            '/company/1',
            $data,
            array(),
            array(
                'HTTP_Authorization' => array(
                    'token' => 'Bearer '.$this->token,
                ),
            )
        );

        $this->assertEquals(Codes::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testLinkCategoryAction()
    {
        $resourceLink1 = $this->getContainer()->get('router')->generate(
            'get_category',
            array('categoryId' => 1),
            UrlGeneratorInterface::RELATIVE_PATH
        );

        $resourceLink2 = $this->getContainer()->get('router')->generate(
            'get_category',
            array('categoryId' => 2),
            UrlGeneratorInterface::RELATIVE_PATH
        );

        $links[] = '<http://nagoda.api/'.$resourceLink1.'>; rel="categories"';
        $links[] = '<http://nagoda.api/'.$resourceLink2.'>; rel="categories"';

        $stringLink = implode(',', $links);

        $this->client->request(
            'LINK',
            '/company/2',
            array(),
            array(),
            array(
                'HTTP_Link' => $stringLink,
                'HTTP_Authorization' => array(
                    'token' => 'Bearer '.$this->token,
                ),
            )
        );

        $this->assertEquals(Codes::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testGetCompanyAction()
    {
        $this->client->request(
            'GET',
            '/company',
            array(),
            array(),
            array(
                'HTTP_Authorization' => array(
                    'token' => 'Bearer '.$this->token,
                ),
            )
        );

        $this->assertEquals(Codes::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        $result = json_decode($this->client->getResponse()->getContent());

        $this->assertObjectHasAttribute('company', $result->details);
    }
}