<?php

namespace UserBundle\Controller\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class RegistrationControllerTest
 * @package UserBundle\Controller
 */
class RegistrationControllerTest extends WebTestCase
{

    /**
     * @var Client
     */
    protected $client;

    /**
     *
     */
    public function setUp()
    {
        static::runCommand('doctrine:schema:drop', array('--force' => true, 'env' => 'test'));
        static::runCommand('doctrine:schema:create', array('env' => 'test'));

        $this->client = static::createClient();
    }

    /**
     *
     */
    public function testPostUserAction()
    {
        $data = array(
            'password' => 'test password',
            'username' => 'test username',
            'is_seller' => false,
        );

        $this->client->request('POST', '/registration', $data);

        $this->assertEquals(Codes::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testPostUserWithErrorAction()
    {
        $data = array(
            'password' => '',
            'username' => '',
            'is_seller' => false,
        );

        $this->client->request('POST', '/registration', $data);

        $this->assertEquals(Codes::HTTP_NOT_ACCEPTABLE, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testPostSubUserAction()
    {
        $data = array(
            'password' => 'test password',
            'username' => 'test username',
            'parentUser' => '1',
        );

        $this->client->request('POST', '/subuser', $data);

        $this->assertEquals(Codes::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }

    /**
     *
     */
    public function testPostSubUserWithErrorAction()
    {
        $data = array(
            'password' => 'test password',
            'username' => 'test username',
            'parentUser' => '',
        );

        $this->client->request('POST', '/subuser', $data);

        $this->assertEquals(Codes::HTTP_NOT_ACCEPTABLE, $this->client->getResponse()->getStatusCode());

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
    }
}