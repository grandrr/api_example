<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 *
 * @Annotation
 */
class CompanyOwner extends Constraint
{

    public $message = 'User not owner of company';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}