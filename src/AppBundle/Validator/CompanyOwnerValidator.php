<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


/**
 * Class CompanyOwnerValidator
 * @package AppBundle\Validator
 */
class CompanyOwnerValidator extends ConstraintValidator
{

    use ContainerAwareTrait;

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $company = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Company\Company')
            ->findOneBy(
                array(
                    'id' => $value,
                    'user' => $user
                )
            );

        if (!$company) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}