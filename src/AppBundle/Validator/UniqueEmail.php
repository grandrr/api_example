<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 *
 * @Annotation
 */
class UniqueEmail extends Constraint
{

    public $message = 'Email %unique% exist';

    public function validatedBy()
    {
        return 'UniqueEmail';
    }
}