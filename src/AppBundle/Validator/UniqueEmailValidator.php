<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class UniqueEmailValidator extends ConstraintValidator
{

    use ContainerAwareTrait;

    public function validate($value, Constraint $constraint)
    {
        /** @var \Doctrine\ORM\EntityRepository $em */
        $em = $this->container->get('doctrine.orm.entity_manager')->getRepository('UserBundle:User');

        $query = $em->createQueryBuilder('u');
        $query->where('u.email = :email');
        $params = ['email'=>$value];

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->context->getRoot();
        if($form->has('id')) {
            $id = $form->get('id')->getViewData();
            $query->andWhere('u.id != :id');
            $params['id']=$id;
        }

        $query->setParameters($params);
        if($query->getQuery()->getOneOrNullResult()) {
            $this->context->buildViolation($constraint->message, ['%unique%' => $value])
                          ->addViolation();
        }
    }
}